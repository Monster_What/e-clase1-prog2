public class Oficial extends Operario {

    private String turno;

    public Oficial() {
    }

    public Oficial(String turno){
        this.turno = turno;
    }
    
    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

}
