/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alvaro
 */
public class Usuarios {

    public static void main(String[] args) {
        Operario operario =  new Operario();
        operario.setNombre("Juan Castro");
        operario.setRuta("San Jose");
        System.out.println("------OPERARIOS ------");
        System.out.println("Nombre: " + operario.getNombre());
        System.out.println("Ruta: " + operario.getRuta());
        Directivo directivo = new Directivo();
        directivo.setNombre("Valeria Sanchez");
        directivo.setDepartamento("Informatica");
        System.out.println("------DIRECTIVOS ------");
        System.out.println("Nombre: " + directivo.getNombre());
        System.out.println("Departamento: " + directivo.getDepartamento());
        Oficial oficial = new Oficial();
        oficial.setNombre("Karla Pineda");
        oficial.setTurno("Noche");
        System.out.println("--------OFICIALES ------");
        System.out.println("Nombre: " + oficial.getNombre());
        System.out.println("Turno: " + oficial.getTurno());
        Tecnico tecnico = new Tecnico();
        tecnico.setNombre("Carlos Perez");
        tecnico.setArea("Mantenimiento");
        System.out.println("-------TÉCNICOS -------");
        System.out.println("Nombre: " + tecnico.getNombre());
        System.out.println("Area: " + tecnico.getArea());
    }
    
    
}
