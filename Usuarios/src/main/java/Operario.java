public class Operario extends Empleado {
    
    private String ruta;

    public Operario() {
    }

    public Operario(String ruta){
        this.ruta = ruta;
    }
    
    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

}
