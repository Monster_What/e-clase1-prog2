public class Tecnico extends Operario {

    private String area;

    public Tecnico() {
    }

    public Tecnico(String area){
        this.area = area;
    }
    
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }


}
