public class Directivo extends Empleado {

    private String departamento;

    public Directivo() {
    }

    public Directivo(String departamento){
        this.departamento = departamento;
    }
    
    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }


}
